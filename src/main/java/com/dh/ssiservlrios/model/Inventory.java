package com.dh.ssiservlrios.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Inventory extends ModelBase {
    private String warehouse;
    @OneToOne(targetEntity = Item.class)
    private Item item;
    private Integer cant;
    private String status;

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public Integer getCant() {
        return cant;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setCant(Integer cant) {
        this.cant = cant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
