package com.dh.ssiservlrios.model;

import javax.persistence.Entity;

@Entity
public class Incident extends ModelBase{
    private String name;
    private Integer code;
    private String subCatInci;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getSubCatInci() {
        return subCatInci;
    }

    public void setSubCatInci(String subCatInci) {
        this.subCatInci = subCatInci;
    }
}
