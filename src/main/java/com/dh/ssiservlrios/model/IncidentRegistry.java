package com.dh.ssiservlrios.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class IncidentRegistry extends ModelBase{
    @OneToOne(optional = false)
    private Incident incident;
    private Date incidentDate;

    @OneToOne(optional = false)
    private Employee employee;
    private String cause;
    private String infraction;
    private Integer quantification;

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getInfraction() {
        return infraction;
    }

    public void setInfraction(String infraction) {
        this.infraction = infraction;
    }

    public Integer getQuantification() {
        return quantification;
    }

    public void setQuantification(Integer quantification) {
        this.quantification = quantification;
    }
}
