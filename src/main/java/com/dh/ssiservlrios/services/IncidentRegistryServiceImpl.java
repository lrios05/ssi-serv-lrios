package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.IncidentRegistry;
import com.dh.ssiservlrios.repositories.IncidentRegistryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentRegistryServiceImpl extends GenericServiceImpl<IncidentRegistry> implements IncidentRegistryService {
    private IncidentRegistryRepository repository;

    public IncidentRegistryServiceImpl(IncidentRegistryRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<IncidentRegistry, Long> getRepository() {
        return repository;
    }
}
