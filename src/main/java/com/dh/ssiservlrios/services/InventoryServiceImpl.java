package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Inventory;
import com.dh.ssiservlrios.repositories.InventoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class InventoryServiceImpl extends GenericServiceImpl<Inventory> implements InventoryService {
    private InventoryRepository repository;

    public InventoryServiceImpl(InventoryRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Inventory, Long> getRepository() {
        return repository;
    }
}
