package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Incident;
import com.dh.ssiservlrios.repositories.IncidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentServiceImpl extends GenericServiceImpl<Incident> implements IncidentService {
    private IncidentRepository repository;

    public IncidentServiceImpl(IncidentRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Incident, Long> getRepository() {
        return repository;
    }
}
