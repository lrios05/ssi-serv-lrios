package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {
}
