package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Category;

import java.util.List;

public interface CategoryService extends GenericService<Category> {
    List<Category> findByCode(String code);
}
