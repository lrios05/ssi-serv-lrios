package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Inventory;

public interface InventoryService extends GenericService<Inventory> {
}
