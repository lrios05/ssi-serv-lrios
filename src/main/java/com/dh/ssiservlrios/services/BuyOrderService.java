package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.BuyOrder;

public interface BuyOrderService extends GenericService<BuyOrder> {
}
