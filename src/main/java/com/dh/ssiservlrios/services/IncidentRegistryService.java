package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.IncidentRegistry;

public interface IncidentRegistryService extends GenericService<IncidentRegistry> {
}
