package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Employee;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public interface EmployeeService extends GenericService<Employee> {
    void saveImage(Long id, InputStream inputStream);
}
