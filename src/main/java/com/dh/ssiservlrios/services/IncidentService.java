package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Incident;

public interface IncidentService extends GenericService<Incident> {
}
