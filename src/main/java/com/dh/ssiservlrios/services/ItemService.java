package com.dh.ssiservlrios.services;

import com.dh.ssiservlrios.model.Item;
import org.springframework.web.multipart.MultipartFile;

public interface ItemService extends GenericService<Item> {
    void saveImage(Long id, MultipartFile file);
}