package com.dh.ssiservlrios.repositories;

import com.dh.ssiservlrios.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
