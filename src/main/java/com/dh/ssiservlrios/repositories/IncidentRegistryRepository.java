package com.dh.ssiservlrios.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservlrios.model.IncidentRegistry;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
}
