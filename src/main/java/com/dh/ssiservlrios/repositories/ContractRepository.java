package com.dh.ssiservlrios.repositories;

import com.dh.ssiservlrios.model.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
