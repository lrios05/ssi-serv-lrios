package com.dh.ssiservlrios.repositories;

import com.dh.ssiservlrios.model.Category;
import com.dh.ssiservlrios.model.SubCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
    Optional<List<Category>> findByCode(String code);
}
