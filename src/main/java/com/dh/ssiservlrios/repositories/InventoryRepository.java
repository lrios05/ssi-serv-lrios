package com.dh.ssiservlrios.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservlrios.model.Inventory;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
}
