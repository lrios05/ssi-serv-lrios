package com.dh.ssiservlrios.repositories;

import com.dh.ssiservlrios.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
