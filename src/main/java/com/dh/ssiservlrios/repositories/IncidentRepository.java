package com.dh.ssiservlrios.repositories;

import org.springframework.data.repository.CrudRepository;
import com.dh.ssiservlrios.model.Incident;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}
