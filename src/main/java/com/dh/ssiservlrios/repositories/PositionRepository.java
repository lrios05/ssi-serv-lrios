package com.dh.ssiservlrios.repositories;

import com.dh.ssiservlrios.model.Position;
import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
