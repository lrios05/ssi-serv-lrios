package com.dh.ssiservlrios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsiServLriosApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsiServLriosApplication.class, args);
    }
}
